package sheridan;

/*
 * 
 * @author C.Jack Jozwik
 * 
 * */

public class Celsius {
	
	public static int fromFahrenheit(int num) {
		int celsius = (int) Math.round(((num - 32.0) * 5 / 9.0));
		
		return celsius;
	}

}
