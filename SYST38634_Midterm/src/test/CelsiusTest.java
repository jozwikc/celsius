package test;

import static org.junit.Assert.*;

import org.junit.Test;
import sheridan.Celsius;

/*
 * 
 * @author C.Jack Jozwik
 * 
 * */

public class CelsiusTest {
	
	@Test
	public void testCelsiusRegular() {
		//41 = 5 C
		int celsiusTemp = Celsius.fromFahrenheit(41);
		
		assertTrue("The tempature provided does not match the result", celsiusTemp == 5);
	}
	
	@Test
	public void testCelsiusException() {
		//Exception (negative)
		int celsiusTemp = Celsius.fromFahrenheit(-41);
		
		assertFalse("The tempature provided does not match the result", celsiusTemp > 0);
	}
	
	@Test
	public void testCelsiusBoundaryIn() {
		//43 = 6.1 C -> BI if it rounds with in
		int celsiusTemp = Celsius.fromFahrenheit(43);
		
		assertTrue("The tempature provided does not match the result", celsiusTemp == 6);
	}
	
	@Test
	public void testCelsiusBoundaryOut() {
		//43 = 6.1 C -> BO if it rounds out
		int celsiusTemp = Celsius.fromFahrenheit(43);
		
		assertFalse("The tempature provided does not match the result", celsiusTemp == 7);
	}

}
